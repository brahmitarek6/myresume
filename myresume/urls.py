from django.contrib import admin
from django.urls import path
from django.urls.conf import re_path

from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static

from myresume.views import MyResumeView,RedirectDetailsView, project_details


schema_view = get_schema_view(
    openapi.Info(
        title="API",
        default_version='v1',
        description="API documentation"
    ),
    public=False,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('HelloMyRes/', admin.site.urls),
    path('', MyResumeView.as_view(), name='myresume'),
    path('projects/<str:proj_pk>', project_details, name='projectdetails'),
    path('project/<str:project_pk>/', RedirectDetailsView.as_view(), name='redirect'),

    # url(r'', include('myresume.app.urls', namespace='myresume')),
    re_path(r'^swagger(?P<format>)$', schema_view.without_ui(
        cache_timeout=0), name='schema-json'),
    path("swagger/", schema_view.with_ui('swagger',
         cache_timeout=0), name='schema-swagger-ui')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
handler404 = "myresume.views.page_not_found_view"
handler500 = "myresume.views.server_error_view"