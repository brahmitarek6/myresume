from django.shortcuts import redirect, render
from django.views.generic import TemplateView
from myresume.app.models import *

def project_details(request,proj_pk):
    template_name = "project_details.html"
    proj=Project.objects.get(pk=int(proj_pk))
    context = {
        'project':proj,
        "resume":MyResumePDF.objects.all().first().path,
        "photo_proj":proj.photos.all().first(),
        "me":ME.objects.first()

    }
    return render(request, template_name, context=context)

class RedirectDetailsView(TemplateView):
    template_name = "redirect.html"
    def get(self, request,**kwargs):
        return redirect("projectdetails",proj_pk=kwargs['project_pk'])


class MyResumeView(TemplateView):
    template_name = "resume.html"
    def get(self, request, *args, **kwargs):
        context = {
            "projects_completed":Project.objects.all().filter(completed=True),
            "projects_incompleted":Project.objects.all().filter(completed=False),
            "resume":MyResumePDF.objects.all().first(),
            "me":ME.objects.first()
        }

        return render(request, self.template_name, context=context)

def page_not_found_view(request, exception):
    return render(request, '44.html', status=404)
def server_error_view(request, *args, **argv):
    return render(request, '500.html', status=505)

