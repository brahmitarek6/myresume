from rest_framework import serializers
from .models import *


class EducationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Education
        fields = '__all__'


class CertificateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Certificate
        fields = '__all__'
class RecommendationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recommendation
        fields = '__all__'

class WorkExperienceSerializer(serializers.ModelSerializer):
    recommendations=RecommendationSerializer(many=True)
    class Meta:
        model = WorkExperience
        fields = '__all__'
class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = '__all__'
class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = '__all__'

class LinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Link
        fields = '__all__'
class IMAGESerializer(serializers.ModelSerializer):
    class Meta:
        model = IMAGE
        fields = '__all__'

class ProjectSerializer(serializers.ModelSerializer):
    tags=SkillSerializer(many=True)
    photos=IMAGESerializer(many=True)
    links=LinkSerializer(many=True)
    
    class Meta:
        model = Project
        fields = '__all__'


        