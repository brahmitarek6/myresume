from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import *
app_name='app'
router = DefaultRouter()
router.register('educations', EducationView, basename='all_educations_list')
router.register('certificates', CertificateView,
                basename='all_certificate_list')
router.register('recommendations', RecommendationView,
                basename='all_recommendation_list')
router.register('workexperiences', WorkExperienceView,
                basename='all_workexperience_list')

router.register('services', ServiceView, basename='all_service_list')
router.register('skills', SkillView, basename='all_skill_list')
router.register('links', LinkView, basename='all_link_list')
router.register('images', IMAGEView, basename='all_image_list')
router.register('projects', ProjectView, basename='all_service_list')
urlpatterns = [
    path('', include(router.urls)),
]
