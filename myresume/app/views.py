from django.shortcuts import render
from rest_framework import viewsets

from .serializers import *
class EducationView( viewsets.ModelViewSet):
    http_method_names = ['get', ]
    serializer_class = EducationSerializer
    def get_queryset(self):
        return Education.objects.all()
class CertificateView( viewsets.ModelViewSet):
    http_method_names = ['get', ]
    serializer_class = CertificateSerializer
    def get_queryset(self):
        return Certificate.objects.all()

class RecommendationView( viewsets.ModelViewSet):
    http_method_names = ['get', ]
    serializer_class = RecommendationSerializer
    def get_queryset(self):
        return Recommendation.objects.all()

class WorkExperienceView( viewsets.ModelViewSet):
    http_method_names = ['get', ]
    serializer_class = WorkExperienceSerializer
    def get_queryset(self):
        return WorkExperience.objects.all()       
class ServiceView( viewsets.ModelViewSet):
    http_method_names = ['get', ]
    serializer_class = ServiceSerializer
    def get_queryset(self):
        return Service.objects.all()       
class SkillView( viewsets.ModelViewSet):
    http_method_names = ['get', ]
    serializer_class = SkillSerializer
    def get_queryset(self):
        return Skill.objects.all()       
class LinkView( viewsets.ModelViewSet):
    http_method_names = ['get', ]
    serializer_class = LinkSerializer
    def get_queryset(self):
        return Link.objects.all()       
class IMAGEView( viewsets.ModelViewSet):
    http_method_names = ['get', ]
    serializer_class = IMAGESerializer
    def get_queryset(self):
        return IMAGE.objects.all()       
class ProjectView( viewsets.ModelViewSet):
    http_method_names = ['get', ]
    serializer_class = ProjectSerializer
    def get_queryset(self):
        return Project.objects.all()       