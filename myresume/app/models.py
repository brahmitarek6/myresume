from django.db.models.deletion import CASCADE
from django.db.models.fields.files import FieldFile
from django.db.models.fields.related import ForeignKey, ManyToManyField, OneToOneField
from model_utils.models import TimeStampedModel
from django.db import models

class Education(TimeStampedModel):
    id= models.BigAutoField(primary_key=True, serialize=False, verbose_name='Id')
    name= models.CharField(max_length=225, verbose_name='Education name')
    from_where= models.CharField(max_length=225, verbose_name='Education from where')
    description= models.TextField(verbose_name='Education description')
    date_take= models.DateField()
    image_of_deplom= models.ImageField(blank=True, null=True, upload_to='images/deploms/', verbose_name='Education deplom')

    class Meta:
        unique_together = ('id', 'name')
        verbose_name ="Education"
        ordering=["date_take"]
    def __str__(self) -> str:
        return f"ED({self.name})"
class Certificate(TimeStampedModel):
    id= models.BigAutoField(primary_key=True, serialize=False, verbose_name='Id')
    name= models.CharField(max_length=225, verbose_name='Certificate name')
    from_where= models.CharField(max_length=225, verbose_name='Certificate from where')
    description= models.TextField(verbose_name='Certificate description')
    date_take= models.DateField()
    image_of_certif= models.ImageField(blank=True, null=True, upload_to='images/certs/', verbose_name='Certificate deplom')
    class Meta:
        unique_together = ('id', 'name')
        verbose_name ="Certificate"
        ordering=["date_take"]
    def __str__(self) -> str:
        return f"CERT({self.name})"

class FromWho(TimeStampedModel):
    id= models.BigAutoField(primary_key=True, serialize=False, verbose_name='Id')
    from_who_name= models.CharField(max_length=225, verbose_name='FromWho name')
    from_who_image= models.ImageField(blank=True, null=True, upload_to='images/fromwho/', verbose_name='FromWho image')
    def __str__(self) -> str:
        return f"FROMWHO({self.from_who_name})"
class Language(TimeStampedModel):
    id= models.BigAutoField(primary_key=True, serialize=False, verbose_name='Id')
    language_name= models.CharField(max_length=225, verbose_name='Language name')
    degree= models.IntegerField(default=50,verbose_name='Language degree')
    def __str__(self) -> str:
        return f"{self.language_name}:{self.degree}%"
    def get_lang_degree(self):
        return f"{self.degree}%"
class Recommendation(TimeStampedModel):
    id= models.BigAutoField(primary_key=True, serialize=False, verbose_name='Id')
    from_who= models.OneToOneField(on_delete=CASCADE,to="app.FromWho",related_name='FromWho')
    description= models.TextField(verbose_name='Recommendation description')
    date_of_recom= models.DateField()
    feedback=models.IntegerField(default=1,verbose_name='Recommendation feedback')
    class Meta:
        verbose_name ="Recommendation"
        ordering = ['date_of_recom']
    def __str__(self) -> str:
        return f"REC({self.id})"
    @property
    def get_rate_str(self):
        return ''.join([str(i) for i in list(range(self.feedback))])
class Service(TimeStampedModel):
    id= models.BigAutoField(primary_key=True, serialize=False, verbose_name='Id')
    name= models.CharField(max_length=225, verbose_name='Service name')
    description= models.TextField(verbose_name='Service description')
    class Meta:
        verbose_name ="Service"
    def __str__(self) -> str:
        return f"SERVICE({self.name})"

class WorkExperience(TimeStampedModel):
    id= models.BigAutoField(primary_key=True, serialize=False, verbose_name='Id')
    name= models.CharField(max_length=225, verbose_name='WorkExperience name')
    with_who= models.CharField(max_length=225, verbose_name='WorkExperience with who ')
    description= models.TextField(verbose_name='WorkExperience description')
    date_of_work= models.DateField()
    recommendations= models.ManyToManyField(related_name='Recommendation', to='app.Recommendation')
    class Meta:
        unique_together = ('id', 'name')
        verbose_name ="WorkExperience"
        ordering = ['date_of_work']
        
    def __str__(self) -> str:
        return f"WE({self.name})"
class Link(models.Model):
    url=models.URLField()
    inGitHub=models.BooleanField(default=False)
class IMAGE(models.Model):
    image=models.ImageField()

class Skill(TimeStampedModel):
    id= models.BigAutoField(primary_key=True, serialize=False, verbose_name='Id')
    name= models.CharField(max_length=225, verbose_name='Skill name')
    sector= models.CharField(max_length=225, verbose_name='Skill sector')
    lavel=models.CharField(choices=[('EXPERT', 'EXPERT'), ('ADVANCED', 'ADVANCED'), ('UPPER-INTERMEDIATE', 'UPPER-INTERMEDIATE'), ('INTERMEDIATE', 'INTERMEDIATE'), ('ELEMENTARY', 'ELEMENTARY'), ('BEGINNER', 'BEGINNER')], default='BEGINNER', max_length=225, verbose_name='Skill lavel')
    class Meta:
        unique_together = ('id', 'name')
        verbose_name ="Skill"
    def __str__(self) -> str:
        return f"SKILL({self.name})"
class HardSkill(TimeStampedModel):
    id= models.BigAutoField(primary_key=True, serialize=False, verbose_name='Id')
    degree= models.IntegerField(default=50,verbose_name='HardSkills degree') # percent
    skills=models.ManyToManyField(related_name='MyHardSkills', to='app.Skill')
    def __str__(self) -> str:
        return f"{', '.join([skill.name for skill in self.skills.all()])}"
class Knowledge(TimeStampedModel):
    id= models.BigAutoField(primary_key=True, serialize=False, verbose_name='Id')
    skills=models.ManyToManyField(related_name='MyKnowledgeForSkills', to='app.Skill')
    def __str__(self) -> str:
        return f"{', '.join([skill.name for skill in self.skills.all()])}"
class Project(TimeStampedModel):
    id= models.BigAutoField(primary_key=True, serialize=False, verbose_name='Id')
    slog= models.CharField(max_length=225, verbose_name='Project slog')
    description= models.TextField(verbose_name='Project description')
    description_markDown= models.TextField(default="",verbose_name='Project description MarkedDown')
    description_2= models.TextField(default="",verbose_name='Project description2')
    completed=models.BooleanField(default=False)
    date_of_start= models.DateField()
    date_of_ends= models.DateField()
    links= models.ManyToManyField(related_name='Links', to='app.Link')
    photos=models.ManyToManyField(related_name='Images', to='app.IMAGE')
    tags=models.ManyToManyField(related_name='Skills', to='app.Skill')
    class Meta:
        unique_together = ('id', 'slog')
        verbose_name ="Project"
        ordering=["date_of_ends"]
    def __str__(self) -> str:
        return f"PROJECT({self.id})"
class MyResumePDF(TimeStampedModel):
    path=models.FileField(blank=True, null=True, upload_to='resums/', verbose_name='Resum')
class Occupation(TimeStampedModel):
    name= models.CharField(max_length=225, verbose_name='Occupation name')
    date_of_start= models.DateField()
    date_of_ends= models.DateField()

class ME(models.Model):
    id= models.BigAutoField(primary_key=True, serialize=False, verbose_name='Id')
    age = models.IntegerField(null=True, blank=True,verbose_name='ME age')
    fullname= models.CharField(null=True, blank=True,default="",max_length=225, verbose_name='ME full name')
    
    email= models.EmailField(blank=True,null=True,verbose_name='ME email')
    phone= models.CharField(null=True, blank=True,default="",max_length=225, verbose_name='ME phone number')
    country= models.CharField(null=True, blank=True,default=None,max_length=225, verbose_name='ME country')
    street= models.CharField(null=True, blank=True,default=None,max_length=225, verbose_name='ME street')
    city= models.CharField(null=True, blank=True,default=None,max_length=225, verbose_name='ME city')

    myphoto=OneToOneField(IMAGE,on_delete=CASCADE,related_name="IMAGE",verbose_name="ME image",blank=True,null=True,default=None)

    experienceyears=models.IntegerField(blank=True,null=True,default=1,verbose_name='ME experienceyears number')

    linkedin= models.URLField(blank=True,null=True,max_length=225, verbose_name='ME url linkendin')
    facebook= models.URLField(blank=True,null=True,max_length=225, verbose_name='ME url facebook')
    github= models.URLField(blank=True,null=True,max_length=225, verbose_name='ME url github')
    gitlab= models.URLField(blank=True,null=True,max_length=225, verbose_name='ME url gitlab')
    twitter= models.URLField(blank=True,null=True,max_length=225, verbose_name='ME url twitter')
    
    occupations=models.ManyToManyField(blank=True,related_name='Occupations', to='app.Occupation')
    languages=models.ManyToManyField(blank=True,related_name='Languages', to='app.Language')
    resums=models.ManyToManyField(blank=True,related_name='Resums', to='app.MyResumePDF')
    
    skills=models.ManyToManyField(blank=True,related_name='MySkills', to='app.Skill')
    
    hardskills=models.ManyToManyField(blank=True,related_name='MyHardSkills', to='app.HardSkill')
    knowledges=models.ManyToManyField(blank=True,related_name='MyKnowledges', to='app.Knowledge')
    
    projects=models.ManyToManyField(blank=True,related_name='Projects', to='app.Project')
    certificates=models.ManyToManyField(blank=True,related_name='Certificates', to='app.Certificate')
    educations=models.ManyToManyField(blank=True,related_name='Educations', to='app.Education')
    workexperiences=models.ManyToManyField(blank=True,related_name='WorkExperiences', to='app.WorkExperience')
    recommendations= models.ManyToManyField(blank=True,related_name='Recommendations', to='app.Recommendation')    
    services= models.ManyToManyField(blank=True,related_name='Services', to='app.Service')
    def __str__(self) -> str:
        return f"I'M({self.fullname})"
    @property
    def summary_occupations(self):
        return ", ".join([occ.name for occ in self.occupations.all()])
    @property
    def summary_educations(self):
        return f'I was studing {", ".join([edu.name for edu in self.educations.all()])} from respectivly {", ".join([edu.description for edu in self.educations.all()])}'
    @property
    def summary_workexperiences(self):
        return f'I work {", ".join([f"as {work.name} at {work.with_who} " for work in self.workexperiences.all()])}'
    @property
    def summary_skills(self):
        return f'I have {len(self.skills.all())} skills like {", ".join([skill.name for skill in self.skills.all()[:2]])} and more others '
    @property
    def summary_projects(self):
        return f'I have made more than {len(self.projects.all())} projects'
    @property
    def summary(self):
        # I"m Brahmi Tarek i born in 1998-09-17 ,I"m curently Full Stack Developer, Embedded System Software Engineer ,I was studing Bachelor\'s degree, Engineer\'s degree from respectivly Institut Supérieur des Sciences Appliquées et Technologie de Mahdia ---\r\nCYCLE PRÉPARATOIRE : MATHÉMATIQUES - PHYSIQUES, Faculté des Sciences Mathématiques, Physiques et Naturelles de Tunis ---\r\nElectrical and Electronics Engineering ,Also I work as Full Stak developer at Confledis  because I have 3 skills like python, Javascript and more others  for that I have made more than 3 projects
        return f'I"m {self.fullname} i born in {self.birthday} ,I"m curently {self.summary_occupations} ,{self.summary_educations} ,Also {self.summary_workexperiences} because {self.summary_skills} for that {self.summary_projects}'
