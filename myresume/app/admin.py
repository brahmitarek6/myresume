from django.contrib.admin import ModelAdmin, site
from django.db.models.base import Model
from .models import *
class EducationAdmin(ModelAdmin):
    list_display = ('id', 'name', 'from_where', 'description','date_take',"image_of_deplom")

class CertificateAdmin(ModelAdmin):
    list_display = ('id', 'name', 'from_where', 'description','date_take',"image_of_certif")

class RecommendationAdmin(ModelAdmin):
    list_display = ('id', 'from_who', 'description','date_of_recom','feedback')
class WorkExperienceAdmin(ModelAdmin):
    list_display = ('id', 'name', 'with_who', 'description','date_of_work')
class ServiceAdmin(ModelAdmin):
    list_display = ('id', 'name',  'description')
class LanguageAdmin(ModelAdmin):
    list_display = ('id', 'language_name', 'degree')
class SkillAdmin(ModelAdmin):
    list_display = ('id', 'name', 'sector','lavel')
class LinkAdmin(ModelAdmin):
    list_display = ('id', 'url','inGitHub')

class IMAGEAdmin(ModelAdmin):
    list_display = ('id', 'image')
class RESUMEAdmin(ModelAdmin):
    list_display = ('id', 'path')
class FromWhoAdmin(ModelAdmin):
    list_display = ('id', 'from_who_name','from_who_image')
class ProjectAdmin(ModelAdmin):
    list_display = ('id', 'slog','description',"completed",'description_markDown','description_2','date_of_start','date_of_ends')
class MEAdmin(ModelAdmin):
    list_display = ('id', 'fullname','age',"country","street","city","experienceyears",'email','phone','linkedin','facebook','gitlab','github',"twitter",'myphoto')
class OccupationAdmin(ModelAdmin):
    list_display = ('id', 'name','date_of_start','date_of_ends')
class KnowledgeAdmin(ModelAdmin):
    list_display = ('id',)
class HardSkillsAdmin(ModelAdmin):
    list_display = ('id',"degree")
site.register(Education, EducationAdmin)
site.register(Certificate, CertificateAdmin)
site.register(Recommendation, RecommendationAdmin)
site.register(WorkExperience, WorkExperienceAdmin)
site.register(Service,ServiceAdmin)
site.register(Skill,SkillAdmin)
site.register(Link,LinkAdmin)
site.register(IMAGE,IMAGEAdmin)
site.register(Project,ProjectAdmin)
site.register(MyResumePDF,RESUMEAdmin)
site.register(ME,MEAdmin)
site.register(Occupation,OccupationAdmin)
site.register(FromWho,FromWhoAdmin)
site.register(Language,LanguageAdmin)
site.register(HardSkill,HardSkillsAdmin)
site.register(Knowledge,KnowledgeAdmin)
